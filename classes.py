class NotPositiveNumber(ValueError):
    pass


class NotNumberError(ValueError):
    pass


class MissingDataError(ValueError):
    pass


class InvalidObjectsInList(AttributeError):
    pass


class InvalidObjectError(AttributeError):
    pass


class InvalidTupleTimeFormat(TypeError):
    pass


class station:
    """
    Contains basic information about stations like:
    id: unique id for of importing from file
    name: name of the station
    pos: position of a station in tens of meters
    stop_duration: duration of the stop in seconds
    """
    def __init__(self, id, name, posx: int, posy: int, stop_duration=None):
        if stop_duration is None:
            stop_duration = 15
        if not name:
            raise ValueError('Missing name')
        self._name = name
        if not id:
            raise ValueError('Missing id')
        self._id = str(id)
        self.pos = int(posx), int(posy)
        try:
            if int(stop_duration) < 0:
                raise ValueError
            self.stop_duration = int(stop_duration)
        except TypeError("stop_duration has to be a whole number"):
            pass
        except ValueError("stop_duration has to be a whole number"):
            pass

    def get_id(self):
        return self._id

    def change_id(self, id):
        if id:
            self.id = id

    def change_name(self, name):
        if not name:
            raise ValueError("Name cannot be empty")
        self._name = name

    def change_pos(self, posx, posy):
        try:
            self.pos = int(posx), int(posy)
        except ValueError:
            raise NotNumberError("Positions have to be numbers")

    def change_stop_duration(self, seconds):
        try:
            if seconds < 0:
                raise ValueError
            self.stop_duration = int(seconds)
        except Exception:
            msg = "stop_duration has to be a positive number"
            raise NotPositiveNumber(msg)

    def get_name(self):
        return self._name

    def __str__(self) -> str:
        return f'{self._name} {self.stop_duration} {self.pos}'

    def present_name(self):
        return f"  | {self._name}"


class line:
    """
    Contains all information about a line:
    id: unique id for importing from file
    name: name of the line
    station_list: list of stations on the line, along with their data

    class also contains a variety of functions to display and change
    stations along with a function to return a position
    when given time from departure
    """
    def __init__(self, id, name, station_list: list):
        if not name:
            raise ValueError("Missing name")
        self.name = name
        if not id:
            raise ValueError("Missing id")
        self._id = str(id)
        try:
            self.stations = station_list
            self.calculate_time()
        except AttributeError:
            msg = "station_list has to be a list of stations"
            raise InvalidObjectsInList(msg)

    def get_id(self):
        return self._id

    def change_id(self, id):
        if id:
            self.id = id

    def calculate_time(self):
        # position is in tens of meters
        # average tram speed is 10m/s
        times = [0]
        idx = 0
        while idx < len(self.stations):
            pos1 = self.stations[idx].pos
            pos2 = self.stations[(idx + 1) % len(self.stations)].pos
            times.append(abs(pos1[0]-pos2[0]) + abs(pos1[1]-pos2[1]))
            idx += 1
        self.times = times

    def add_station(self, station: station):
        try:
            self.stations.append(station)
            self.calculate_time()
        except AttributeError:
            raise InvalidObjectError("station has to be a station")

    def del_station(self, index):
        try:
            del self.stations[index]
            self.calculate_time()
        except AttributeError:
            raise InvalidObjectError("station has to be a station")

    def print_stations(self):
        for station in self.stations:
            print(station)

    def change_name(self, name):
        if not name:
            raise ValueError("Name cannot be empty")
        self.name = name

    def line_time(self):
        total_time_in_seconds = sum(self.times)
        for station in self.stations:
            total_time_in_seconds += station.stop_duration
        return total_time_in_seconds

    def time_to_seconds(self, time):
        return time[0]*60*60 + time[1]*60 + time[2]

    def seconds_to_time(self, seconds):
        hours = seconds // 3600
        minutes = (seconds % 3600) // 60
        seconds = seconds % 60
        return hours, minutes, seconds

    def process_time_seconds(self, time_in_seconds, ending_time=False):
        # procures time in a XX:XX:XX format
        # if ending_time, function returns time of departure from last stop
        if ending_time:
            time_in_seconds += self.line_time()
        new_time = self.seconds_to_time(time_in_seconds)
        return f'{new_time[0]:2}:{new_time[1]:02}:{new_time[2]:02}'

    def process_time(self, time, ending_time=False):
        time_in_seconds = self.time_to_seconds(time)
        return self.process_time_seconds(time_in_seconds, ending_time)

    def construct_description(self, time):
        description = ''
        if len(self.stations) > 2:
            beginning = self.stations[0].get_name()
            end = self.stations[len(self.stations)-1].get_name()
            description = f'{beginning} - {end}\n'
        description += self.process_time(time, False)
        description += f' - {self.process_time(time, True)}\n'
        description += 'Arrival  Departure | station\n'
        return description

    def list_of_stations(self, time):
        station_list = []
        current_time = self.time_to_seconds(time)
        time_idx = 0
        first = True
        for station in self.stations:
            if not first:
                current_time += self.times[time_idx]
                time_idx += 1
            first = False
            current_time = current_time % (60*60*24)
            str_station = self.process_time_seconds(current_time, False)
            current_time += station.stop_duration
            current_time = current_time % (60*60*24)
            str_station += ' '
            str_station += self.process_time_seconds(current_time, False)
            str_station += station.present_name()
            station_list.append(str_station)
        return station_list

    def range_check(self, number, range):
        number = int(number)
        if number >= range or number < 0:
            raise ValueError("Time must be numbers from 00:00 to 23:59")

    def time_check(self, time):
        try:
            self.range_check(time[0], 24)
            self.range_check(time[1], 60)
        except TypeError:
            raise InvalidTupleTimeFormat('time has to be a 2-part tuple')

    def generate_timetable(self, time: tuple):
        # time is a 2-part tuple: (HH, MM)
        self.time_check(time)
        time = (time[0], time[1], 0)
        # changing time to (HH, MM, SS) format
        timetable = f'{self.name}\n'
        timetable += self.construct_description(time)
        station_list = self.list_of_stations(time)
        for station in station_list:
            timetable += f'{station}\n'
        return timetable

    def ask_position(self, seconds):
        current_time = 0
        time_idx = 1
        first = True
        prev_pos = None
        dis_from_station = seconds
        for station in self.stations:
            if not first:
                current_time += self.times[time_idx]
                dis_from_station -= self.times[time_idx - 1]
                time_idx += 1
            if seconds < current_time:
                next_pos = station.pos
                act_pos = [prev_pos[0], prev_pos[1]]
                height = 1
                width = 1
                if act_pos[0] > next_pos[0]:
                    width = -1
                if act_pos[1] > next_pos[1]:
                    height = -1
                while dis_from_station > 0:
                    if act_pos[0] != next_pos[0]:
                        act_pos[0] += width
                    else:
                        act_pos[1] += height
                    dis_from_station -= 1
                return f"Moving to: {station.get_name()}", act_pos
            first = False
            current_time += station.stop_duration
            dis_from_station -= station.stop_duration
            if seconds < current_time:
                return f"Current stop: {station.get_name()}", station.pos
            prev_pos = station.pos
        return "Course completed", self.stations[-1].pos


class tram:
    """
    N0TE: trams are NOT imported from files, they are created on course init
    Contains basic information about a Tram, like:
    id: unique id for organising purposes
    line: line the tram is assigned to
    starting_time: time of arrival at first stop of a line
    position: position in a line (text)(on which stop/ going to which stop)
    line_time: time in seconds from starting_time
    """
    def __init__(self, id, starting_time: tuple, line: line):
        if not id:
            raise ValueError("Missing id")
        self._id = str(id)
        try:
            self.line = line
            self.line.time_check(starting_time)
        except AttributeError:
            raise InvalidObjectError("Line has to be a line")
        self.s_time = starting_time
        self.line_time = 0
        self.position = 'Not started'
        self.pos = self.line.stations[0].pos

    def change_id(self, id):
        if not id:
            raise ValueError("Missing id")
        self._id = str(id)

    def get_id(self):
        return self._id

    def change_line(self, line):
        try:
            self.line = line
        except AttributeError:
            raise InvalidObjectError("Line has to be a line")

    def change_position(self, position):
        self.position = position

    def move(self, seconds):
        if self.position != "Course completed" and seconds != 0:
            self.line_time += seconds
            self.position, self.pos = self.line.ask_position(self.line_time)


class course:
    """
    Contains basic information about a Course, like:
    id:  unique id for of importing from file
    line: line the course is assigned to
    departure_times: list of times at which a tram departs on a line
    current_time: current time in seconds of the simulation (from 00:00)
        if no time is given, time is set to 5:00
    """
    def __init__(self, id, line: line, departure_times: list, current_time=-1):
        if not id:
            raise ValueError('Missing id')
        self._id = str(id)
        try:
            self.line = line
            self.time_check(departure_times)
        except AttributeError:
            raise InvalidObjectError("Line has to be a line")
        self.d_times = departure_times
        self.create_trams()
        if current_time == -1:
            current_time = 60*60*5
        if type(current_time) == tuple:
            format_time = (current_time[0], current_time[1], 0)
            self.current_time = self.line.time_to_seconds(format_time)
        else:
            self.current_time = int(current_time)

    def get_id(self):
        return self._id

    def time_check(self, times):
        for time in times:
            self.line.time_check(time)
        for x in range(len(times) - 1):
            time1 = int(times[x][0])*60*60 + int(times[x][1])*60
            time2 = int(times[x + 1][0])*60*60 + int(times[x + 1][1])*60
            if time1 > time2:
                raise ValueError('Times have to be rising from 00:00 to 23:59')

    def change_id(self, id):
        if not id:
            raise ValueError('Missing id')
        self._id = str(id)

    def change_current_time(self, current_time):
        if type(current_time) == tuple:
            format_time = (current_time[0], current_time[1], 0)
            self.current_time = self.line.time_to_seconds(format_time)
        else:
            self.current_time = int(current_time)

    def change_departure_times(self, departure_times):
        self.time_check(departure_times)
        self.d_times = departure_times

    def create_trams(self):
        trams = []
        id_counter = 1
        for time in self.d_times:
            id = f'{str(id_counter):2} {self._id:2}'
            tramx = tram(id, time, self.line)
            id_counter += 1
            trams.append(tramx)
        self.trams = trams

    def move_time(self, time: int):
        self.current_time += time
        for tram in self.trams:
            tram_time = int(tram.s_time[0])*60*60 + int(tram.s_time[1])*60
            if tram_time < self.current_time:
                if tram.position == 'Not started':
                    tram.move(self.current_time - tram_time)
                else:
                    tram.move(time)

    def present_trams_neatly(self):
        trams = f'{self.line.name}\n'
        for tram in self.trams:
            trams += f'{int(tram.get_id()):2} {tram.position}\n'
        return trams

    def present_active_trams_neatly(self):
        trams = f'\n{self.line.name}'
        statuses = ['Course completed', 'Not started']
        none = True
        for tram in self.trams:
            if tram.position not in statuses:
                trams += f'\nId:{int(tram.get_id()):2}  {tram.position}'
                none = False
        if none:
            return ''
        return trams

    def present_active_trams(self):
        trams = []
        statuses = ['Course completed', 'Not started']
        for tram in self.trams:
            if tram.position not in statuses:
                trams.append((tram.get_id(), tram.pos[0], tram.pos[1]))
        return trams

    def ids_of_finished_trams(self):
        ids = []
        for tram in self.trams:
            if tram.position == 'Course completed':
                ids.append(tram.get_id())
        return ids

    def present_current_time(self):
        return self.line.seconds_to_time(self.current_time)

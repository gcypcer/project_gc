import classes
import pytest


def test_station_init():
    stacja = classes.station(1, "stacja", 0, 0)
    assert stacja.get_id() == '1'
    assert stacja.pos == (0, 0)
    assert stacja.stop_duration == 15
    assert stacja.get_name() == "stacja"
    assert str(stacja) == 'stacja 15 (0, 0)'


def test_station_change_name():
    stacja = classes.station(1, "stacja", 0, 0)
    stacja.change_name("stacja")
    assert stacja.get_name() == "stacja"
    with pytest.raises(ValueError):
        stacja.change_name("")


def test_station_init_errors():
    with pytest.raises(ValueError):
        stacja = classes.station(1, "", 0, 0)
    with pytest.raises(ValueError):
        stacja = classes.station(1, "", 0, 0)
    with pytest.raises(TypeError):
        stacja = classes.station(1, "yes", 0, 0, "yes")
    with pytest.raises(TypeError):
        stacja = classes.station(1, "yes", 0, 0, -1)
    stacja = classes.station(1, "stacja", 0, 0)
    stacja = stacja


def test_change_stop_duration():
    stacja = classes.station(1, "stacja", 0, 0)
    stacja.change_stop_duration(6)
    assert stacja.stop_duration == 6
    with pytest.raises(classes.NotPositiveNumber):
        stacja.change_stop_duration("yes")
    with pytest.raises(classes.NotPositiveNumber):
        stacja.change_stop_duration(-1)


def test_station_change_pos():
    stacja = classes.station(1, "stacja", 0, 0)
    stacja.change_pos(5, 5)
    assert stacja.pos == (5, 5)
    with pytest.raises(classes.NotNumberError):
        stacja.change_pos("yes", 5)


def test_line_init():
    s1 = classes.station(1, 's1', 0, 0)
    s2 = classes.station(2, 's2', 1, 1)
    my_line = classes.line(1, "my line", [s1, s2])
    assert my_line.get_id() == '1'
    assert my_line.name == 'my line'
    assert my_line.stations == [s1, s2]
    my_line.change_name('my_line')
    assert my_line.name == 'my_line'


def test_line_init_errors():
    s1 = classes.station(1, 's1', 0, 0)
    s2 = classes.station(2, 's2', 1, 1)
    my_line = classes.line(1, "my line", [s1, s2])
    with pytest.raises(classes.InvalidObjectsInList):
        my_line = classes.line(1, "my line", ["y", s2])
    with pytest.raises(ValueError):
        my_line = classes.line(1, "", [s1, s2])
    with pytest.raises(ValueError):
        my_line.change_name('')


def test_line_manage_stations():
    s1 = classes.station(1, 's1', 0, 0)
    s2 = classes.station(2, 's2', 1, 1)
    my_line = classes.line(1, "my line", [s1, s2])
    my_line.del_station(1)
    assert my_line.stations == [s1]
    my_line.add_station(s2)
    assert my_line.stations == [s1, s2]


def test_line_calculate_time():
    s1 = classes.station(1, 's1', 0, 0)
    s2 = classes.station(2, 's2', 1, 1)
    my_line = classes.line(1, "my line", [s1, s2])
    assert my_line.times == [2, 2]


def test_line_line_time():
    s1 = classes.station(1, 's1', 0, 0)
    s2 = classes.station(2, 's2', 1, 1)
    my_line = classes.line(1, "my line", [s1, s2])
    assert my_line.line_time() == 30 + 4


def test_line_time_check():
    my_line = classes.line(1, "my line", [])
    with pytest.raises(classes.InvalidTupleTimeFormat):
        my_line.time_check((0))
    with pytest.raises(ValueError):
        my_line.time_check((-1, 1))
    my_line.time_check((1, 1, 1))


def test_line_ask_position():
    s1 = classes.station(1, 's1', 0, 0)
    s2 = classes.station(2, 's2', 1, 1)
    my_line = classes.line(1, "my line", [s1, s2])
    assert my_line.ask_position(0)[0] == "Current stop: s1"
    assert my_line.ask_position(16)[0] == "Moving to: s2"
    assert my_line.ask_position(20)[0] == "Current stop: s2"
    assert my_line.ask_position(40)[0] == "Course completed"


def test_tram_init():
    s1 = classes.station(1, 's1', 0, 0)
    s2 = classes.station(2, 's2', 1, 1)
    my_line = classes.line(1, "my line", [s1, s2])
    my_tram = classes.tram(1, (0, 0), my_line)
    assert my_tram.get_id() == '1'
    assert my_tram.line == my_line
    assert my_tram.s_time == (0, 0)
    assert my_tram.line_time == 0
    assert my_tram.position == 'Not started'


def test_tram_changing():
    s1 = classes.station(1, 's1', 0, 0)
    s2 = classes.station(2, 's2', 1, 1)
    line1 = classes.line(1, "line1", [s1, s2])
    line2 = classes.line(2, "line2", [s2])
    my_tram = classes.tram(1, (0, 0), line1)
    my_tram.change_id(2)
    assert my_tram.get_id() == '2'
    my_tram.change_position('here')
    assert my_tram.position == 'here'
    my_tram.change_line(line2)
    assert my_tram.line == line2


def test_tram_move():
    s1 = classes.station(1, 's1', 0, 0)
    s2 = classes.station(2, 's2', 1, 1)
    my_line = classes.line(1, "my line", [s1, s2])
    my_tram = classes.tram(1, (0, 0), my_line)
    my_tram.move(0)
    assert my_tram.position == 'Not started'
    my_tram.move(1)
    assert my_tram.position == 'Current stop: s1'
    my_tram.move(10)
    assert my_tram.position == 'Current stop: s1'
    my_tram.move(5)
    assert my_tram.position == 'Moving to: s2'
    my_tram.move(10)
    assert my_tram.position == 'Current stop: s2'
    my_tram.move(10)
    assert my_tram.position == 'Course completed'


def test_course_init():
    s1 = classes.station(1, 's1', 0, 0)
    s2 = classes.station(2, 's2', 1, 1)
    my_line = classes.line(1, "my line", [s1, s2])
    departure_times = [(0, 0), (0, 1), (12, 0)]
    my_course = classes.course(1, my_line, departure_times)
    assert my_course.get_id() == '1'
    assert my_course.line == my_line
    assert my_course.d_times == departure_times
    assert my_course.current_time == 60*60*5
    assert my_course.present_current_time() == (5, 0, 0)


def test_course_changing():
    s1 = classes.station(1, 's1', 0, 0)
    s2 = classes.station(2, 's2', 1, 1)
    my_line = classes.line(1, "my line", [s1, s2])
    departure_times = [(0, 0), (0, 1), (12, 0)]
    my_course = classes.course(1, my_line, departure_times)
    my_course.change_id(2)
    assert my_course.get_id() == '2'
    my_course.change_current_time(10)
    assert my_course.current_time == 10
    my_course.change_current_time((0, 2))
    assert my_course.current_time == 120
    my_course.change_departure_times([(0, 0)])
    assert my_course.d_times == [(0, 0)]


def test_course_move_time():
    s1 = classes.station(1, 's1', 0, 0)
    s2 = classes.station(2, 's2', 1, 1)
    my_line = classes.line(1, "my line", [s1, s2])
    departure_times = [(5, 0), (5, 1), (12, 0)]
    my_course = classes.course(1, my_line, departure_times)
    my_course.move_time(30)
    tram_1 = my_course.trams[0]
    assert tram_1.position == 'Current stop: s2'
    my_course.move_time(35)
    tram_2 = my_course.trams[1]
    assert tram_1.position == 'Course completed'
    assert tram_2.position == 'Current stop: s1'


def test_course_invalid_line():
    my_line = 1
    departure_times = [(5, 0), (5, 1), (12, 0)]
    with pytest.raises(classes.InvalidObjectError):
        classes.course(1, my_line, departure_times)

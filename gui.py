from PySide2.QtWidgets import (
    QApplication,
    QMainWindow,
    QGraphicsSimpleTextItem,
    QGraphicsScene
)
from PySide2.QtGui import QPainter, QBrush
from PySide2.QtCore import Qt, QTimer
import sys
from ui_mainwindow import Ui_TramOverview
import classes_io


class TramOverviewWindow(QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        self._ui = Ui_TramOverview()
        self._ui.setupUi(self)
        self.timer = QTimer()
        self.timer.setInterval(1024)
        self.time = 5*60*60
        self.speed = 1
        self._active_trams = {}
        self._station_labels = []
        self.setup_data()

    def setup_data(self):
        self._scene = QGraphicsScene()
        self._ui.mainMap.setScene(self._scene)
        self._ui.mainMap.setRenderHint(QPainter.Antialiasing)
        self.stations = classes_io.import_stations()
        self.lines = classes_io.import_lines(self.stations)
        self.courses = classes_io.import_courses(self.lines)
        self.labels = True
        for station in self.stations:
            x, y = station.pos
            marker = self._scene.addEllipse(-4, -4, 8, 8)
            marker.setBrush(QBrush(Qt.red))
            marker.setPos(x*2, -y*2)
            marker.setFlag(marker.ItemIgnoresTransformations)
            text_item = QGraphicsSimpleTextItem(station.get_name(), marker)
            text_item.setPos(5, -7)
            font = text_item.font()
            font.setPointSize(font.pointSize()-1)
            text_item.setFont(font)
            self._station_labels.append(text_item)

        self._ui.startButton.clicked.connect(self.start_stop)
        self._ui.labelButton.clicked.connect(self.label_toggle)
        self._ui.fasterButton.clicked.connect(self.faster)
        self._ui.slowerButton.clicked.connect(self.slower)
        self.timer.timeout.connect(self.move)

    def label_toggle(self):
        if self.labels:
            for label in self._station_labels:
                label.setVisible(False)
                self.labels = False
        else:
            for label in self._station_labels:
                label.setVisible(True)
                self.labels = True

    def format_time(self):
        hours = int(self.time // 3600)
        minutes = int((self.time % 3600) // 60)
        seconds = int(self.time % 60)
        return f'{hours:2}:{minutes:02}:{seconds:02}'

    def add_trams(self, active_trams):
        for tram in active_trams:
            x = int(tram[1])
            y = int(tram[2])
            if tram[0] in self._active_trams.keys():
                marker = self._active_trams[tram[0]]
                marker.setPos(x*2, -y*2)
            else:
                marker = self._scene.addEllipse(-3, -3, 6, 6)
                marker.setBrush(QBrush(Qt.blue))
                marker.setPos(x*2, -y*2)
                marker.setFlag(marker.ItemIgnoresTransformations)
                text_item = QGraphicsSimpleTextItem(tram[0], marker)
                text_item.setPos(0, 0)
                font = text_item.font()
                font.setPointSize(font.pointSize()-1)
                text_item.setFont(font)
                self._active_trams[tram[0]] = marker

    def remove_finished_trams(self, finished_ids):
        for key in self._active_trams.keys():
            if key in finished_ids:
                self._active_trams[key].setOpacity(0)
                del self._active_trams[key]

    def move(self):
        self._ui.clock.setText(self.format_time())
        self.time += 1
        self.time = self.time % (24*60*60)
        if self.time == 5*60*60:
            self._active_trams = {}
            self._station_labels = []
            self.courses = classes_io.import_courses(self.lines)
        for course in self.courses:
            course.move_time(1)
            self.remove_finished_trams(course.ids_of_finished_trams())
            self.add_trams(course.present_active_trams())

    def start_stop(self):
        if not self.timer.isActive():
            self.timer.start(self.timer.interval())
        else:
            self.timer.stop()

    def faster(self):
        if self.speed < 4096:
            self.speed *= 2
            self.timer.setInterval(1024/self.speed)
            self._ui.speed.setText(f'{self.speed}x')

    def slower(self):
        if self.speed > 1:
            self.speed /= 2
            self.timer.setInterval(1024/self.speed)
            self._ui.speed.setText(f'{int(self.speed)}x')


def guiMain(args):
    app = QApplication(args)
    window = TramOverviewWindow()
    window.show()
    return app.exec_()


if __name__ == "__main__":
    guiMain(sys.argv)

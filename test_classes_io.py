import classes_io
import pytest


def test_import_stations_basic():
    classes_io.import_stations()


def test_import_stations_changed():
    classes_io.import_stations('backup_stations.txt')


def test_import_lines_basic():
    stations = classes_io.import_stations()
    classes_io.import_lines(stations)


def test_import_lines_changed():
    stations = classes_io.import_stations('backup_stations.txt')
    classes_io.import_lines(stations, 'backup_lines.txt')


def test_import_courses_basic():
    stations = classes_io.import_stations()
    lines = classes_io.import_lines(stations)
    classes_io.import_courses(lines)


def test_import_courses_changed():
    stations = classes_io.import_stations('backup_stations.txt')
    lines = classes_io.import_lines(stations, 'backup_lines.txt')
    classes_io.import_courses(lines, 'backup_courses.txt')


def test_import_nonexsistant():
    with pytest.raises(classes_io.FilePathNotFound):
        classes_io.import_stations('nonexistant')

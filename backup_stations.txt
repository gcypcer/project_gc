id,name,posx,posy,stop_duration
1,Glebocka,60,90,15
2,Ostrodzka,60,80,15
3,Kamykowa,50,75,10
4,Echa,50,65,10
5,Pozarowa,-48,97,15
6,Budowlana,-78,38,20
7,Wyszogrodzka,-86,52,15
8,Chodecka,-3,80,20
9,Turmoncka,7,-78,25
10,Danusi,92,-76,25
11,Bohuna,52,95,25
12,Berensona,86,-55,30
13,Wierna,99,19,30
14,Bukowiecka,-11,-55,10
15,Rzeczna,-24,-74,25
16,Hutnicza,-84,12,15
17,Chemiczna,-38,-44,30
18,Fizyczna,91,-79,25
19,Baleya,-66,37,25
20,Korsaka,-7,-43,10
21,Naczelnikowska,-93,-19,15
22,Adampolska,-84,-74,15
23,Annopol,-6,35,25
24,AWF,-62,66,30
25,Baletowa,-86,-70,25
26,Gabiego,-42,87,15
27,Masztowa,8,-23,30
28,Bleaka,31,59,30
29,Statkowa,-95,-23,15
30,Piracka,-83,41,15
31,Skarbowa,98,-70,20
32,Wodnista,23,40,20
33,Chomikowa,-75,79,30
34,Piosenkowa,100,-79,20
35,Armatnia,21,22,20
36,Wynuchowa,-4,66,10
37,Myszogrodzka,-28,-81,20
38,Robakowa,88,-71,20
39,Mostowa,-59,-77,15
40,Topielcowa,-75,1,15
41,Nawiedzona,-16,86,20
42,Czekoladowa,34,74,10
43,Angielska,100,83,20
44,Beczkowa,48,60,30
45,Butelkowa,-70,-87,10
46,Faktowa,-86,-32,20
47,Ustawiona,99,40,30
48,Belgradzka,-98,-71,15
49,Moskiewska,-36,-42,10
50,Biedronki,87,-17,25
51,Robactwa,-36,10,15
52,Bielawa,-3,72,10
53,Brzeziny,-87,-95,15
54,Burakowa,20,87,25
55,Bukowiecka,-52,33,20
56,Bystra,-1,-36,15
57,Ceglana,34,-53,10
58,Cementowa,46,-46,30
59,Betoniarki,-54,-37,10
60,Metalowa,98,97,30
61,Drewniana,70,-13,25
62,Celulozy,91,-33,15
63,Calowa,49,90,30
64,Centrum,5,0,30
65,CH Marki,-61,-95,25
66,Cicha,37,71,25
67,Cmentarz,92,61,30
68,Conrada,7,17,10
69,Coopera,64,-70,15
70,Saurona,27,46,20
71,Sarumana,4,-47,30
72,Hobbita,76,21,30
73,Bangalore,-79,-14,20
74,Ficsita,-42,31,30
75,Czumy,-55,0,10
76,Dawidy,78,96,25
77,Chewbacki,-43,-57,30
78,Darwina,-91,36,20
79,Dawidy,-85,-34,20
80,Diamentowa,-14,40,25
81,Dolna,2,2,25
82,Dobra,13,-4,15
83,Draceny,-68,-5,10
84,Grabina,44,10,10
85,Gryfitów,-41,-70,10
86,Grzybowa,-18,-66,15
87,ZUS,-54,88,25
88,Karowa,-25,-61,25
89,Kolumba,27,-3,25
90,Koszykowa,57,96,30
91,Komety,-54,80,15
92,Klonowa,-62,34,15
93,Klaudyny,83,52,30
94,Kondracka,8,57,15
95,Kondratowicza,-52,-26,30
96,Korczaka,35,57,10
97,Kosiarzy,-40,-38,30
98,Laurowa,55,-81,10
99,Lelewela,59,-15,15
100,Linin,11,-83,25
101,Lenina,44,-51,25
102,Stalina,26,8,20
103,Lipowa,-29,80,10
104,Meblowa,52,-31,25
105,Michalin,44,24,20
106,Metryczna,14,-59,20
107,Mazurska,-55,91,20
108,Modra,-89,-66,20
109,Niwa,-7,-70,20
110,Nizinna,78,77,25

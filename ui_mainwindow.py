# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'mainwindow.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

from mapview import MapView


class Ui_TramOverview(object):
    def setupUi(self, TramOverview):
        if not TramOverview.objectName():
            TramOverview.setObjectName(u"TramOverview")
        TramOverview.resize(630, 548)
        self.centralwidget = QWidget(TramOverview)
        self.centralwidget.setObjectName(u"centralwidget")
        self.verticalLayout = QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.startButton = QPushButton(self.centralwidget)
        self.startButton.setObjectName(u"startButton")

        self.horizontalLayout.addWidget(self.startButton)

        self.fasterButton = QPushButton(self.centralwidget)
        self.fasterButton.setObjectName(u"fasterButton")

        self.horizontalLayout.addWidget(self.fasterButton)

        self.slowerButton = QPushButton(self.centralwidget)
        self.slowerButton.setObjectName(u"slowerButton")

        self.horizontalLayout.addWidget(self.slowerButton)

        self.labelButton = QPushButton(self.centralwidget)
        self.labelButton.setObjectName(u"labelButton")

        self.horizontalLayout.addWidget(self.labelButton)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.speed = QLabel(self.centralwidget)
        self.speed.setObjectName(u"speed")

        self.horizontalLayout.addWidget(self.speed)

        self.clock = QLabel(self.centralwidget)
        self.clock.setObjectName(u"clock")

        self.horizontalLayout.addWidget(self.clock)


        self.verticalLayout.addLayout(self.horizontalLayout)

        self.mainMap = MapView(self.centralwidget)
        self.mainMap.setObjectName(u"mainMap")

        self.verticalLayout.addWidget(self.mainMap)

        TramOverview.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(TramOverview)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 630, 20))
        TramOverview.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(TramOverview)
        self.statusbar.setObjectName(u"statusbar")
        TramOverview.setStatusBar(self.statusbar)

        self.retranslateUi(TramOverview)

        QMetaObject.connectSlotsByName(TramOverview)
    # setupUi

    def retranslateUi(self, TramOverview):
        TramOverview.setWindowTitle(QCoreApplication.translate("TramOverview", u"TramOverview", None))
        self.startButton.setText(QCoreApplication.translate("TramOverview", u"I> / II", None))
        self.fasterButton.setText(QCoreApplication.translate("TramOverview", u"x2", None))
        self.slowerButton.setText(QCoreApplication.translate("TramOverview", u"/2", None))
        self.labelButton.setText(QCoreApplication.translate("TramOverview", u"Toggle Labels", None))
        self.speed.setText(QCoreApplication.translate("TramOverview", u"1x", None))
        self.clock.setText(QCoreApplication.translate("TramOverview", u" 5:00:00", None))
    # retranslateUi


import csv
from classes import station, line, course


class FilePathNotFound(FileNotFoundError):
    pass


class FilePermissionError(PermissionError):
    pass


class FilePathIsDirectory(IsADirectoryError):
    pass


class InvalidDataInFile(csv.Error):
    pass


def file_check(file, is_importing=True):
    try:
        if is_importing:
            status = 'r'
        else:
            status = 'w'
        with open(file, status):
            pass
    except FileNotFoundError:
        raise FilePathNotFound('Could not open database')
    except PermissionError:
        msg = 'You do not have permission to open the database'
        raise FilePermissionError(msg)
    except IsADirectoryError:
        msg = 'Path needs to lead to a file, not a directory'
        raise FilePathIsDirectory(msg)


def import_stations(file='stations.txt'):
    file_check(file)
    stations = []
    with open(file, 'r') as file_handle:
        reader = csv.DictReader(file_handle)
        try:
            for row in reader:
                id = row['id']
                name = row['name']
                posx = row['posx']
                posy = row['posy']
                stop_duration = row['stop_duration']
                s1 = station(id, name, posx, posy, stop_duration)
                stations.append(s1)
        except csv.Error:
            raise InvalidDataInFile("Data needs to be in csv")
    return stations


def import_lines(stations, file='lines.txt'):
    file_check(file)
    lines = []
    station_dict = {}
    for statioon in stations:
        station_dict[statioon.get_id()] = statioon
    with open(file, 'r') as file_handle:
        reader = csv.DictReader(file_handle)
        try:
            for row in reader:
                id = row['id']
                name = row['name']
                station_ids = row['station_ids']
                station_ids = str(station_ids).split()
                new_ids = []
                for st_id in station_ids:
                    new_ids.append(''.join(e for e in st_id if e.isalnum()))
                # first = True
                # for stid in station_ids:
                #     if first:
                #         new_ids.append(str(stid[2]))
                #         first = False
                #     else:
                #         new_ids.append(str(stid[1]))
                temp_stations = []
                for st_id in new_ids:
                    temp_stations.append(station_dict[st_id])
                temp_line = line(id, name, temp_stations)
                lines.append(temp_line)
        except csv.Error:
            raise InvalidDataInFile("Data needs to be in csv")
    return lines


def import_courses(lines, file='courses.txt'):
    file_check(file)
    courses = []
    lines_dict = {}
    for linee in lines:
        lines_dict[linee.get_id()] = linee
    with open(file, 'r') as file_handle:
        reader = csv.DictReader(file_handle)
        try:
            for row in reader:
                id = row['id']
                line_id = row['line_id']
                temp_line = lines_dict[line_id]
                departure_times = row['departure_times'].split(', ')
                new_times = []
                for d_time in departure_times:
                    new_times.append(''.join(e for e in d_time if e.isalnum()))
                idx = 0
                newer_times = []
                while idx < len(new_times):
                    time = new_times[idx], new_times[idx + 1]
                    newer_times.append(time)
                    idx += 2
                temp_course = course(id, temp_line, newer_times)
                courses.append(temp_course)
        except csv.Error:
            raise InvalidDataInFile("Data needs to be in csv")
    return courses


def export_stations(stations, file='stations.txt'):
    file_check(file, False)
    with open(file, 'w') as file_handle:
        headers = ['id', 'name', 'posx', 'posy', 'stop_duration']
        writer = csv.DictWriter(file_handle, headers)
        writer.writeheader()
        for my_station in stations:
            id = my_station.get_id()
            name = my_station.get_name()
            posx = my_station.pos[0]
            posy = my_station.pos[1]
            stop_duration = my_station.stop_duration
            writer.writerow({
                'id': id,
                'name': name,
                'posx': posx,
                'posy': posy,
                'stop_duration': stop_duration
            })


def export_lines(lines, file='lines.txt'):
    file_check(file, False)
    with open(file, 'w') as file_handle:
        headers = ['id', 'name', 'station_ids']
        writer = csv.DictWriter(file_handle, headers)
        writer.writeheader()
        for my_line in lines:
            id = my_line.get_id()
            name = my_line.name
            station_ids = []
            for statioon in my_line.stations:
                station_ids.append(statioon.get_id())
            writer.writerow({
                'id': id,
                'name': name,
                'station_ids': station_ids
            })


def export_courses(courses, file='courses.txt'):
    file_check(file, False)
    with open(file, 'w') as file_handle:
        headers = ['id', 'line_id', 'departure_times']
        writer = csv.DictWriter(file_handle, headers)
        writer.writeheader()
        for my_course in courses:
            id = my_course.get_id()
            line_id = my_course.line.get_id()
            departure_times = my_course.d_times
            writer.writerow({
                'id': id,
                'line_id': line_id,
                'departure_times': departure_times
            })
